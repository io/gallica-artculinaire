# Analyse de l'évolution de l'iconographie dans un périodique de Gallica: l'art culinaire

V1 complète 2022-08-26

## Présentation du notebook

Ce notebook montre comment mettre en place un traitement d'images numérisées disponibles dans une base de données en ligne (Gallica).

Il décrit toutes les étapes du processus de travail:

1. récupérer les métadonnées et les images en exploitant les API de Gallica,
2. choisir un modèle de détection d'images et de textes,
3. appliquer ce modèle et extraire de ses prédictions des indicateurs.

**Les données utilisées sont les fascicules numérisés de la revue [*L'Art culinaire*](https://catalogue.bnf.fr/ark:/12148/cb344287435) entre 1883 et 1913, [diffusés par Gallica](https://gallica.bnf.fr/ark:/12148/cb344287435/date.item).**

Niveau du notebook: intermédiaire/avancé

Principales notions vues:

* exploration et utilisation d'APIs non-standard et standard,
* téléchargement et manipulation d'un grand corpus d'images,
* sélection d'un modèle prédictif pré-entraîné pour l'analyse d'images,
* application d'un modèle prédictif à un corpus d'images,
* extraction d'indicateurs sur les prédictions,
* visualisation des indicateurs.


## Contenu du notebook

Présentation générale

1. Parcourir et récupérer les données: la revue L'Art culinaire diffusée par Gallica

- Récupérer les années et les fascicules
- Récupérer les informations bibliographiques
- Récupérer la pagination
- Télécharger les vues des fascicules
- Pour aller plus loin

2. Sélectionner un modèle pour analyser la mise en page des fascicules

- Repérer les différents modèles capables d'extraire des images et des illustrations
- Réaliser une première évaluation empirique
- Départager les deux modèles les plus prometteurs

3. Appliquer le modèle à l'ensemble du corpus et exploiter les prédictions

- Visualiser les blocs d'images et de texte détectés
- Calculer l'aire totale des blocs d'images et de texte détectés
- Visualiser les aires totales des blocs d'images et de texte détectés
- Visualiser les ratios entre surfaces d'images et de texte, et leur évolution dans le temps

Conclusion, limitations et perspectives


## Crédits

### Auteurs

Ce notebook a été réalisé par Mathieu Morey et Anthony Gigerich, avec la contribution d'Émilien Schultz et d'Antoine Blanchard.

## Licence

MIT License

Copyright (c) 2022 Huma-Num Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
